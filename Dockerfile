FROM docker.io/caddy:alpine

WORKDIR /srv

COPY index.html .
COPY *.js .

CMD ["caddy", "file-server", "--root", "/srv", "--listen", ":80"]
