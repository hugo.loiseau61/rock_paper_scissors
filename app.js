"use strict"

const width = window.innerWidth // X axis
const height = window.innerHeight // Y axis
const fontSize = parseInt(window.getComputedStyle(document.getElementsByTagName("html")[0], null).getPropertyValue("font-size").slice(0, -2)) + 4

const main = async () => {
    const app = document.getElementById("app")

    let nbOfElements = window.prompt("How many players will fight ?", 100)

    if (nbOfElements === "" || isNaN(nbOfElements)) {
        nbOfElements = 100
    }

    let elements = []
    for (let i = 0; i < nbOfElements; i++) {
        elements.push(newRandomEntity(width-fontSize, height-fontSize, elements))
    }

    while (true) {
        for (let e of elements) {
            e.move()
            e.display()
        }
        await new Promise(r => setTimeout(r, 10));
    }
}

window.addEventListener("load", main)
