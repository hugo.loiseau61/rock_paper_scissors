"use strict"

const rock = "🪨"
const paper = "📃"
const scissors = "✂️"
const waterWell = "💧"

const newEntity = (x, y, velocityX, velocityY, element, elements) => {
    return {
        id: Math.random().toString(),
        element: element,
        position: {
            x: x,
            y: y,
        },
        velocity: {
            x: velocityX,
            y: velocityY,
        },

        move() {
            let nextX = this.position.x + this.velocity.x
            if (nextX > (width - fontSize) || nextX < 0) {
                this.velocity.x *= -1
            }
            this.position.x += this.velocity.x

            let nextY = this.position.y + this.velocity.y
            if (nextY > (height - fontSize) || nextY < 0) {
                this.velocity.y *= -1
            }
            this.position.y += this.velocity.y
            this.fight()
        },

        display() {
            let me = document.getElementById(this.id)
            if (me === null) {
                me = document.createElement("span")
            }

            me.id = this.id
            me.innerHTML = this.element
            me.style.position = "absolute"
            me.style.top = `${this.position.y}px`
            me.style.left = `${this.position.x}px`

            app.appendChild(me)
        },

        fight() {
            for (let e of elements) {
                // Don't check colision with us
                if (e.id === this.id) {
                    continue
                }

                // Check colinding with other entities
                if (
                    e.position.x < this.position.x + fontSize && this.position.x < e.position.x + fontSize &&
                    e.position.y < this.position.y + fontSize && this.position.y < e.position.y + fontSize
                ){
                    if (e.element === rock && this.element === scissors) {
                        this.element = rock
                    } else if (e.element === paper && this.element === rock) {
                        this.element = paper
                    } else if (e.element === scissors && this.element === paper) {
                        this.element = scissors
                    } else if (e.element === waterWell) {
                        this.element = waterWell
                    }
                }
            }
        }
    }
}

const newRandomEntity = (fieldWidth, fieldHeight, elements) => {
    let x = Math.floor(Math.random() * fieldWidth)
    let y = Math.floor(Math.random() * fieldHeight)
    let velocityX = Math.random() * 2 - 1
    let velocityY = Math.sqrt(1 - Math.pow(velocityX, 2)) * (Math.random() > 0.5 ? -1 : 1)

    let elem
    let t = Math.floor(Math.random() * 3)
    switch(t) {
        case 0:
            elem = rock
            break
        case 1:
            elem = paper
            break
        case 2:
            elem = scissors
            break
        default:
            elem = waterWell
    }

    return newEntity(x, y, velocityX, velocityY, elem, elements)
}
