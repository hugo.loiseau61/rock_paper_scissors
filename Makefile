.PHONY: img ctn-run publish

DOCKER ?= podman
REGISTRY ?= docker.io/albandewilde
PORT ?= 8080

img:
	@$(DOCKER) build . --tag $(REGISTRY)/rockpaperscissors

ctn-run:
	@$(DOCKER) run -p $(PORT):80 $(REGISTRY)/rockpaperscissors

publish:
	@$(DOCKER) push $(REGISTRY)/rockpaperscissors
